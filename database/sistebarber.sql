CREATE TABLE IF NOT EXISTS barberia (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL,
  `estado` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS cita (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre_completo` VARCHAR(45) NULL,
  `numero_documento` DOUBLE NULL,
  `fecha_hora` VARCHAR(50) NULL,
  `telefono` VARCHAR(45) NULL,
  `barberia_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cita_barberia_idx` (`barberia_id` ASC),
  CONSTRAINT `fk_cita_barberia`
    FOREIGN KEY (`barberia_id`)
    REFERENCES barberia (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
