import express, { Router } from "express";

import appointmentController from "../controllers/appointmentController";

class AppointmentRoutes {
  router: Router = Router();

  constructor() {
    this.config();
  }

  config() {
    this.router.get("/", appointmentController.list);
    this.router.get("/:id", appointmentController.getOne);
    this.router.get("/barbershop/:id", appointmentController.getByIdBarbershop);
    this.router.post("/exist", appointmentController.getAppointmentExist);
    this.router.post("/", appointmentController.create);
    this.router.put("/:id", appointmentController.update);
    this.router.delete("/:id", appointmentController.delete);
  }
}

export default new AppointmentRoutes().router;
