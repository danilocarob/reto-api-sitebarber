import express, { Router } from 'express';

import barbershopController from '../controllers/barbershopController';

class BarbershopRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {
        this.router.get('/', barbershopController.list);
        this.router.get('/:id', barbershopController.getOne);
        this.router.post('/', barbershopController.create);
        this.router.put('/:id', barbershopController.update);
        this.router.delete('/:id', barbershopController.delete);
    }

}

export default new BarbershopRoutes().router;

