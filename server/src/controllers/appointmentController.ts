import { Request, Response } from "express";

import pool from "../database";

class AppointmentController {
  public async list(req: Request, res: Response): Promise<void> {
    const appointments = await pool.query("SELECT * FROM cita");
    res.json(appointments);
  }

  public async getOne(req: Request, res: Response): Promise<any> {
    const { id } = req.params;
    const appointments = await pool.query("SELECT * FROM cita WHERE id = ?", [
      id,
    ]);
    console.log(appointments.length);
    if (appointments.length > 0) {
      return res.json(appointments[0]);
    }
    res.status(404).json({ text: "La cita no existe" });
  }

  public async getByIdBarbershop(req: Request, res: Response): Promise<any> {
    const { id } = req.params;

    const appointments = await pool.query(
      "SELECT * FROM cita WHERE barberia_id = ?",
      [id]
    );
    console.log(appointments.length);
    if (appointments.length > 0) {
      return res.json(appointments);
    }
    res.status(404).json({ text: "La cita no existe" });
  }

  public async getAppointmentExist(req: Request, res: Response): Promise<any> {
    const oldAppointment = req.body;
    console.log("obj cita", oldAppointment);

    const appointments = await pool.query(
      "SELECT * FROM cita WHERE fecha_hora = ?",
      [oldAppointment.fecha_hora]
    );
    console.log(appointments.length);
    if (appointments.length > 0) {
      return res.json("existe");
    }
    res.status(404).json("no existe");
  }

  public async create(req: Request, res: Response): Promise<void> {
    const result = await pool.query("INSERT INTO cita set ?", [req.body]);
    res.json({ message: "Cita creada" });
  }

  public async update(req: Request, res: Response): Promise<void> {
    const { id } = req.params;
    const oldAppointment = req.body;
    await pool.query("UPDATE cita set ? WHERE id = ?", [req.body, id]);
    res.json({ message: "La cita fue actualizada" });
  }

  public async delete(req: Request, res: Response): Promise<void> {
    const { id } = req.params;
    await pool.query("DELETE FROM cita WHERE id = ?", [id]);
    res.json({ message: "La cita fue eliminada" });
  }
}

const appointmentController = new AppointmentController();
export default appointmentController;
