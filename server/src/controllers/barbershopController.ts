import { Request, Response } from 'express';


import pool from '../database';

class BarbershopController {

    public async list(req: Request, res: Response): Promise<void> {
        const barbershops = await pool.query('SELECT * FROM barberia');
        res.json(barbershops);
    }

    public async getOne(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const barbershops = await pool.query('SELECT * FROM barberia WHERE id = ?', [id]);
        console.log(barbershops.length);
        if (barbershops.length > 0) {
            return res.json(barbershops[0]);
        }
        res.status(404).json({ text: "La barberia no existe" });
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO barberia set ?', [req.body]);
        res.json({ message: 'barberia creada' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldBarbershop = req.body;
        await pool.query('UPDATE barberia set ? WHERE id = ?', [req.body, id]);
        res.json({ message: "La barberia fue actualizada" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM barberia WHERE id = ?', [id]);
        res.json({ message: "La barberia fue eliminada" });
    }
}

const barbershopController = new BarbershopController;
export default barbershopController;